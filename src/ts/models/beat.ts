class Beat {
    public BeatDuration: number;
    public BeatNumber: number;
    public BeatNumber4th: number;
    public BeatNumber8th: number;
    public BeatNumber32th: number;

    public Time: number;
}