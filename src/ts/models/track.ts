/// <reference path="../main.ts"/>
/// <reference path="../../js/workers/processAudioWorker.js"/>

class Track {
    public Name: string;
    public Artist: string;
    public Duration: number;

    public Beats: Beat[];
    public SampleRate: number;
    public Channels: number;
    public BPM: number;
    public BPMOptions: any[];
    public BPMStartTime: number;
    public BeatDuration: number;

    public HTMLElement: HTMLAudioElement;
    public HTMLMainItem: HTMLElement;
    public HTMLListItem: HTMLElement;
    public HTMLCanvasFrequency: HTMLCanvasElement;

    public Settings = {
        SkipBeatFrequencies: 10000,
        Threshold: 0.9,
        LowPassFilterFrequency: 200
    }

    /**
     * LoadFromFile
     */
    public LoadFromFile(file: File) {

        // Set the name
        this.Name = file.name;

        // Instatiate new FileReader
        var reader = new FileReader();

        // Get AudioContext
        var context = EDMixxerMain.AudioContext;

        // Bind function to on load
        reader.onload = () => {
            context.decodeAudioData(reader.result, (buffer: AudioBuffer) => {
                this.Channels = buffer.numberOfChannels;
                this.Duration = buffer.duration;
                this.SampleRate = buffer.sampleRate;

                //this.Prepare(buffer);
                this.Prepare(buffer);
            });
        };

        // Start reading file
        reader.readAsArrayBuffer(file);

        this.CreateHTMLTag(file);
        this.CreateHTMLListItem();
        this.CreateHTMLFrequencyCanvas();
        this.CreateHTMLMainItem();
    }

    public Remove() {
        this.HTMLElement.remove();
        this.HTMLListItem.remove();
        this.HTMLCanvasFrequency.remove();
        this.HTMLMainItem.remove();
    }

    public CreateHTMLListItem() {
        // Create List item
        var item = $("<li/>").addClass("item");

        var image = $("<div/>").addClass("image");
        $(image).css({
            "background-image": "url('static/images/vinyl.png')",
        });

        var title = $("<div/>").addClass("title");
        $(title).html(this.Name);

        var other = $("<div/>").addClass("other");

        var bpm = $("<div/>").addClass("bpm");
        $(bpm).html("?? BPM");

        var duration = $("<div/>").addClass("duration");
        $(duration).html("0:00.000");

        $(other).append(bpm, duration);

        $(item).append(image, title, other);

        this.HTMLListItem = item[0] as HTMLElement;
    }

    public CreateHTMLMainItem() {
        var songBar = $("<div/>").addClass("song");

        var songName = $("<div/>").html(this.Name);

        var controlBox = $("<div/>").addClass("controls");

        var bpmSelect = $("<select/>").addClass("select--bpm");

        var option = $("<option/>");

        $(option).html("?? BPM");

        $(bpmSelect).append(option);

        $(controlBox).append(songName, bpmSelect);

        var spinner = $("<div/>").addClass("spinner");

        var vinyl = $("<div/>").addClass("vinyl rotating");

        $(spinner).append(vinyl);

        var progress = $("<div/>").addClass("progress");

        var bar = $("<div/>").addClass("bar");

        $(bar).append(this.HTMLCanvasFrequency);

        var overlay = $("<div/>").addClass("overlay");

        var current_time = $("<div/>").addClass("time time--current");
        $(current_time).html("0:00.000");

        $(overlay).append(current_time);

        var end_time = $("<div/>").addClass("time time--end");
        $(end_time).html("0:00.000");

        $(bar).append(overlay, end_time);

        $(progress).append(bar);

        var beat_timer = $("<div/>").addClass("beat-timer");

        var beat1 = $("<div/>").addClass("beat beat-1");
        var beat2 = $("<div/>").addClass("beat beat-2");
        var beat3 = $("<div/>").addClass("beat beat-3");
        var beat4 = $("<div/>").addClass("beat beat-4");

        var synch = $("<div/>").addClass("synch");

        $(beat_timer).append(beat1, beat2, beat3, beat4, synch);

        $(songBar).append(controlBox, spinner, progress, beat_timer);

        this.HTMLMainItem = songBar[0] as HTMLElement;
    }

    private CreateHTMLTag(file: File) {
        var html = $("<audio/>").attr("src", URL.createObjectURL(file));
        this.HTMLElement = html[0] as HTMLAudioElement;
    }

    public CreateHTMLFrequencyCanvas() {
        var canvas = $("<canvas/>").addClass("frequency-map")[0] as HTMLCanvasElement;
        this.HTMLCanvasFrequency = canvas;
    }

    public SetMainMusicControl() {

        var bpmSelect = $(this.HTMLMainItem).find(".select--bpm");

        // Clear old options -- if any
        $(bpmSelect).html("");

        // Set BPM Options
        for (var i = 0; i < this.BPMOptions.length; i++) {
            var option = $("<option/>");

            if (i == 0) {
                $(option).html(this.BPMOptions[i].tempo + " BPM - Detected");
            } else {
                $(option).html(this.BPMOptions[i].tempo + " BPM");
            }

            $(option).data("bpm", this.BPMOptions[i]);

            $(bpmSelect).append(option);
        }

        $(bpmSelect).change((e) => {
            var bpm = $((<HTMLSelectElement>e.target).selectedOptions[0]).data("bpm")

            this.GenerateBeats(bpm);
        });

        // Set Time
        var end_time = $(this.HTMLMainItem).find(".time--end");
        $(end_time).html(Time.SecondsToMinuteSecondMilisecondString(this.Duration));
    }

    private updateProgressInterval: number = null;
    private nextBeat: Beat = null;

    /**
     * Play
     */
    public Play() {
        this.HTMLElement.play();

        this.nextBeat = this.Beats[0]; // start with first beat

        var overlay = $(this.HTMLMainItem).find('.overlay');
        var currentTime = $(this.HTMLMainItem).find('.time--current')

        this.updateProgressInterval = setInterval(() => {
            var progress = (this.HTMLElement.currentTime / this.HTMLElement.duration) * 100;

            $(overlay).width(progress + "%");
            $(currentTime).html(Time.SecondsToMinuteSecondMilisecondString(this.HTMLElement.currentTime));

            if (this.HTMLElement.currentTime >= this.nextBeat.Time) {
                // Do some flash animation on the beat thingies
                var beatBox: JQuery;

                switch (this.nextBeat.BeatNumber4th) {
                    case 0:
                        beatBox = $(this.HTMLMainItem).find(".beat-1");
                        break;
                    case 1:
                        beatBox = $(this.HTMLMainItem).find(".beat-2");
                        break;
                    case 2:
                        beatBox = $(this.HTMLMainItem).find(".beat-3");
                        break;
                    case 3:
                        beatBox = $(this.HTMLMainItem).find(".beat-4");
                        break;
                }

                // TODO :: MAKE THIS BETTER xD
                $(beatBox).addClass("active");
                setTimeout(() => {
                    $(beatBox).removeClass("active");
                }, 50); // Flash for 1/20th second

                // Set next beat
                do {
                    var currentBeatIndex = this.Beats.indexOf(this.nextBeat);

                    if (this.Beats.length > currentBeatIndex + 1) {
                        this.nextBeat = this.Beats[currentBeatIndex + 1];
                    } else {
                        break;
                    }
                } while (this.nextBeat.Time < this.HTMLElement.currentTime);

            }

        }, 1000 / 60) // 60x per second
    }

    /**
     * Stop
     */
    public Stop() {
        if (this.updateProgressInterval) {
            clearInterval(this.updateProgressInterval);
        }
    }

    /**
     * SetPlaybackRate
     * @param speed ; the playback speed for this track ( 0.5 - 2.0 )
     * @param duration (optional) ; time in miliseconds to 'transform' to the new speed
     */
    public SetPlaybackRate(speed: number, duration?: number) {

        if (!duration) {
            this.HTMLElement.playbackRate = speed;
        } else {
            createjs.Tween.get(this.HTMLElement, { override: true }).to({ playbackRate: speed }, duration);
        }
    }

    /**
     * Prepare
     */
    public Prepare(buffer: AudioBuffer) {

        var offlineContext = new OfflineAudioContext(buffer.numberOfChannels, buffer.length, buffer.sampleRate);
        // Create buffer source
        var source = offlineContext.createBufferSource();
        source.buffer = buffer;

        // Beats, or kicks, generally occur around the 100 to 150 hz range.
        // Below this is often the bassline.  So let's focus just on that.

        // First a lowpass to remove most of the song.
        var lowpass = offlineContext.createBiquadFilter();
        lowpass.type = "lowpass";
        lowpass.frequency.value = 160;
        lowpass.Q.value = 1;

        // Run the output of the source through the low pass.

        source.connect(lowpass);
        // Now a highpass to remove the bassline.

        var highpass = offlineContext.createBiquadFilter();
        highpass.type = "highpass";
        highpass.frequency.value = 110;
        highpass.Q.value = 1;

        // Run the output of the lowpass through the highpass.
        lowpass.connect(highpass);

        // Run the output of the highpass through our offline context.
        highpass.connect(offlineContext.destination);

        // Start the source, and render the output into the offline conext.
        source.start(0);
        offlineContext.startRendering();
        offlineContext.oncomplete = (e) => {
            this.Process(e.renderedBuffer);
        };
    }

    /**
     * Process
     */
    public Process(buffer: AudioBuffer) {

        var leftChannelBuffer = buffer.getChannelData(0);
        var rightChannelBuffer = buffer.getChannelData(1);

        // Instantiate Worker
        var worker = new Worker(URL.createObjectURL(new Blob(["(" + processAudioWorker.toString() + ")()"], { type: 'text/javascript' })));

        var workerInput = {
            leftChannelBuffer: leftChannelBuffer,
            rightChannelBuffer: rightChannelBuffer,
            sampleRate: this.SampleRate
        }

        worker.postMessage(workerInput);

        // When worker completes
        worker.onmessage = (e) => {
            var returnObject = e.data;

            // Beat Logic
            this.BPM = returnObject.BPM.tempo;
            this.BPMStartTime = returnObject.BPM.position;

            this.BPMOptions = returnObject.BPMGuesses;

            // Draw Frequency Image
            var lineDataArray = returnObject.frequencyMapData;
            var maxVolume = returnObject.maxVolume;

            var multiply = 160 / maxVolume;

            // $(this.HTMLCanvasFrequency).attr({
            //     "width": lineDataArray.length,
            //     "height": 160
            // });

            var tempCanvas = $("<canvas>")[0] as HTMLCanvasElement;
            $(tempCanvas).attr({
                width: lineDataArray.length,
                height: 160
            });

            var ctx = tempCanvas.getContext("2d");
            ctx.lineWidth = 1;
            ctx.strokeStyle = "black";

            lineDataArray.forEach((el: number, i: number) => {
                ctx.moveTo(i, -el * multiply + 80);
                ctx.lineTo(i, el * multiply + 80);
                ctx.stroke();
            });

            var img = ctx.canvas.toDataURL("image/png");

            this.HTMLCanvasFrequency.parentElement.style.backgroundImage = "url(" + img + ")";
            this.HTMLCanvasFrequency.parentElement.style.backgroundPosition = "center";
            this.HTMLCanvasFrequency.parentElement.style.backgroundSize = "100% 100%";

            this.GenerateBeats(this.BPMOptions[0]);

            $(this).trigger("track:processCompleted", this);
        }

        // When processing error occurs
        worker.onerror = (e) => {
            console.log(e);
        }
    }

    public GenerateBeats(bpmObj: any) {

        // Beat Logic
        this.BPM = bpmObj.tempo;
        this.BPMStartTime = bpmObj.position;

        this.BeatDuration = 60 / this.BPM;

        // TODO :: Move this to worker :D
        this.Beats = [];

        // Instantiate Worker
        var worker = new Worker(URL.createObjectURL(new Blob(["(" + generateBeatsWorker.toString() + ")()"], { type: 'text/javascript' })));

        var workerInput = {
            BPM: this.BPM,
            BeatStartTime: this.BPMStartTime,
            BeatDuration: this.BeatDuration,
            SongDurationSeconds: this.HTMLElement.duration
        }

        worker.postMessage(workerInput);

        worker.onmessage = (e) => {
            this.Beats = e.data.Beats;
        }

        worker.onerror = (e) => {
            console.error(e.message);
        }
    }
}