/// <reference path="external/createjs-lib/index.d.ts" />
/// <reference path="external/tweenjs/index.d.ts" />
/// <reference path="external/soundjs/index.d.ts" />
/// <reference path="external/easeljs/index.d.ts" />
/// <reference path="external/jquery/index.d.ts" />
/// <reference path="external/jquery.blockui/index.d.ts" />

/// <reference path="models/beat.ts"/>
/// <reference path="models/track.ts"/>
/// <reference path="modules/music.ts"/>
/// <reference path="modules/ui.ts"/>
/// <reference path="modules/time.ts"/>

class EDMixxerMain {

    public static AudioContext: AudioContext;

    public Music: MusicController;
    public UI: UIController;

    /**
     * Settings used throughout Main
     */
    public Settings = {
        SongDropSelector: "#song-uploader"
    }

    /**
     * Constructor
     */
    constructor() {
        this.Init();
    }

    /**
     * Init
     */
    public Init() {

        this.Music = new MusicController();
        this.UI = new UIController();

        EDMixxerMain.AudioContext = new ((<any>window).AudioContext || (<any>window).webkitAudioContext)();

        this.Bind();

        $(this).trigger("mixer:onInitCompleted", this);
    }

    /**
     * ApplySettings
     */
    public ApplySettings() {
        console.warn("Apply Settings is not implemented yet!");
    }

    /**
     * Bind
     */
    public Bind() {

        // Add Song to playlist
        $(document).on('change', this.Settings.SongDropSelector, (e) => {
            var droppedFiles: File[] = (<any>e.target).files;

            if (!droppedFiles || droppedFiles.length <= 0) { return; }

            // Process each file
            $(droppedFiles).each((i, file: any) => {

                var track = new Track();

                // Add Event listener to event 'processCompleted'
                $(track).on("track:processCompleted", (e, processedTrack: Track) => {

                    // Add processed song to playlist
                    this.Music.AddNewTrack(processedTrack);

                    $(processedTrack.HTMLListItem).unblock();
                });

                // Start loading file
                track.LoadFromFile(file);

                $(this.Music.HTMLPlayList).append(track.HTMLListItem);

                $(track.HTMLListItem).block({
                    message: $("<img/>").attr("src", "static/images/spinner-1.gif").css({
                        width: "50px",
                        height: "50px"
                    }),
                    css: {
                        border: "none",
                        "background-color": "rgba(0,0,0,0)"
                    }
                });
            });
        });
    }
}

/**
 * Start the initialization of EDMixxer
 */
var EDMixxer;
$(function () {
    // On Document Ready
    EDMixxer = new EDMixxerMain();
})