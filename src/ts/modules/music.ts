class MusicController {

    public HTMLPlayList = (() => {
        return $(".playlist .list");
    })();

    public HTMLMainList = (() => {
        return $("#main-board .songs");
    })();

    private Volume: number = 1;

    private CurrentTrack: Track;
    private NextTrack: Track;

    private TransitionBeats: number;

    private IsLocked: boolean;

    private TransitionSpeedCallback : () => void;
    private TransitionCallback : () => void;

    constructor() {

        this.TransitionBeats = 8;
        this.IsLocked = false;

        this.Bind();
    }

    /**
     * Bind
     */
    public Bind() {

        // On document ready
        $(document).on('click', '.playlist .slide-button', function (e) {

            // If menu was closed, open it
            if ($('.playlist').hasClass('open')) {

                $('.playlist').removeClass('open');
            } else {

                $('.playlist').addClass('open');
            }
        });

        $(document).on('click', '.playlist .item', (e) => {
            var item = e.currentTarget;
            var track = $(item).data("track");

            this.SetNext(track);
        });
    }

    /**
     * AddNewTrack
     */
    public AddNewTrack(track: Track) {
        // Append to <main>
        $("main").append(track.HTMLElement);

        // Create List item
        $(track.HTMLListItem).find(".bpm").html(track.BPM + " BPM");
        $(track.HTMLListItem).find(".duration").html(Time.SecondsToMinuteSecondMilisecondString(track.Duration));

        // Set track data to list item
        $(track.HTMLListItem).data("track", track);
    }

    public RemoveTrack(track: Track) {
        var index = $(track.HTMLListItem).index();

        track.Remove();
    }

    public OnEndCurrent() {

        // TODO :: check if property value remains set, and only DOM is changed
        $(this.CurrentTrack.HTMLMainItem).remove();

        this.CurrentTrack.Stop();

        this.SetNext();

        this.CurrentTrack = this.NextTrack;
        this.CurrentTrack.Play();
        this.CurrentTrack.HTMLElement.onended = () => this.OnEndCurrent();
        this.NextTrack = null;
    }

    /**
     * NextTrack
     */
    public SetNext(track?: Track) {

        var playListItems = this.HTMLPlayList.children();

        if (playListItems.length < 1) {
            console.warn("Playlist is empty, can't play the 'Next' track!"); return;
        }

        if (this.IsLocked) {
            console.warn("Can't trigger 'Next' when the music controller is 'locked'! Please wait for the transition to complete."); return;
        }

        if (!track) {
            var currentIndex = $(this.CurrentTrack.HTMLListItem).index();
            var nextIndex = 0;

            if (playListItems.length > currentIndex + 1) {
                nextIndex = currentIndex + 1;
            } else {
                console.info("There was no 'Next' track, switched to the first item in the list.");
            }

            var nextItem = playListItems[nextIndex];

            this.NextTrack = $(nextItem).data("track");
        } else {
            this.NextTrack = track;
        }

        // Set HTML of Main control list 
        this.NextTrack.SetMainMusicControl();
        $(this.HTMLMainList).append(this.NextTrack.HTMLMainItem);

        if (!this.CurrentTrack) {
            this.CurrentTrack = this.NextTrack;
            this.NextTrack = null;

            this.CurrentTrack.HTMLElement.volume = this.Volume;
            this.CurrentTrack.Play();

            this.CurrentTrack.HTMLElement.onended = () => {
                this.OnEndCurrent();
            }

            return;
        }

        // TODO: This is next step shit -- align BPM's of songs and flow to next song
        var currentBPM = this.CurrentTrack.BPM;
        var nextBPM = this.NextTrack.BPM;

        var transitionSpeedMultiplier = nextBPM / currentBPM;

        this.TransitionSpeedCallback = () => {
            this.TransitionSpeedCallback = null;

            var transitionTime = (this.CurrentTrack.BeatDuration * this.TransitionBeats) * transitionSpeedMultiplier; // TODO:: this is not correct!

            createjs.Tween.get(this.CurrentTrack.HTMLElement, { override: true }).to({ playbackrate: transitionSpeedMultiplier }, transitionTime).call(() => {

                this.TransitionCallback = () => {
                    this.TransitionCallback = null;

                    var transitionTime = (this.NextTrack.BeatDuration * this.TransitionBeats);

                    createjs.Tween.get(this.CurrentTrack.HTMLElement, { override: true }).to({ volume: 0 });
                    createjs.Tween.get(this.NextTrack.HTMLElement, { override: true }).to({ volume: this.Volume }).call(() => {
                        this.CurrentTrack = this.NextTrack;
                        this.NextTrack = null;
                    });
                }
            });
        };

        // Transition beats = 4, 8 or 32;

        // Set listener to the current track that triggers on the next beat ( of )

        // FIRST : take transistion beats to match current song BPM to next song BPM ( playbackrate adjust )

        // SECOND : take transition beats to fade to next song ( BPM and timing should match over this time )

        // THIRD : Shift the current en next track, and unlock the music player.

        //this.IsLocked = true;
    }

    public Setvolume(volume: number, immediate: boolean) {
        if (immediate) {
            this.Volume = volume;

            if (this.CurrentTrack) {
                this.CurrentTrack.HTMLElement.volume = volume;
            }
        } else {
            this.Volume = volume;

            if (this.CurrentTrack) {
                createjs.Tween.get(this.CurrentTrack.HTMLElement).to({ volume: volume }, 2000);
            }
        }
    }

    public SetTrackPositionFromPercentage(track: Track, percentage: number) {
        var percDuration = track.HTMLElement.duration / 100;
        var newProgress = percDuration * percentage;

        this.SetTrackPositionFromSeconds(track, newProgress);
    }

    public SetTrackPositionFromSeconds(track: Track, seconds: number) {
        track.HTMLElement.currentTime = seconds;
    }

    // Update method that gets fired every with beat
    public OnBeat(beat: Beat) {
        if (this.TransitionBeats == beat.BeatNumber32th) {
            if (this.TransitionSpeedCallback) {
                this.TransitionSpeedCallback();
            }

            if (this.TransitionCallback) {
                this.TransitionCallback();
            }
        }
    }
}