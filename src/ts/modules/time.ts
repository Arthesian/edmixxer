class Time {
    public static SecondsToMinuteSecondMilisecondString(s : number){
        return Time.MilisecondsToMinuteSecondMilisecondString(s * 1000);
    }

    public static MilisecondsToMinuteSecondMilisecondString(ms : number){
        var ms = Math.round(ms);
        
        var m = Math.floor(ms / 60000); // Minutes
        ms -= m * 60000;
        var s = Math.floor(ms / 1000); // seconds
        ms -= s * 1000;
        return m + ":" + (s < 10 ? '0' + s : s) + "." + ms //zero padding on seconds
    }

    public static SecondsToHourMinutesSecondsString(s : number){
        var h = Math.floor(s / 3600); // Hours
        s -= h * 3600;
        var m = Math.floor(s / 60); // Minutes
        s -= m * 60;
        return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minute and seconds
    }
}