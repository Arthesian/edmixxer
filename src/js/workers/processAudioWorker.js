function processAudioWorker() {
    // scope
    var self = this;

    // Variables to use over multiple functions
    self.variables = {
        sampleRate: 44100,
        samplesPerSecond: 10
    }

    self.addEventListener('message', function(e) {
        // Process data here
        var returnObject = {
            BPM: 0,
            BPMGuesses: [],
            frequencyMapData: [],
            maxVolume: 0
        }

        self.variables.sampleRate = e.data.sampleRate;

        var leftChannelBuffer = e.data.leftChannelBuffer; //buffer.getChannelData(0);
        var rightChannelBuffer = e.data.rightChannelBuffer; //buffer.getChannelData(1);

        var peaks = self.getPeaks([leftChannelBuffer, rightChannelBuffer]);

        var max = peaks.sort(function(a, b) {
            return b.volume - a.volume;
        }).slice(0, 1);

        var divider = Math.round(self.variables.sampleRate / self.variables.samplesPerSecond);

        leftChannelBuffer.forEach((el, i) => {
            if (i % divider === 0) {
                returnObject.frequencyMapData.push((Math.abs(el) + Math.abs(rightChannelBuffer[i])) / 2);
            }
        });

        var groups = self.getIntervals(peaks);

        if (groups.length) {
            var top5 = groups.sort(function(intA, intB) {
                return intB.count - intA.count;
            }).splice(0, 10);

            returnObject.BPMGuesses = top5;
            returnObject.BPM = top5[0];

            returnObject.maxVolume = max[0].volume || 1;
        }

        self.postMessage(returnObject);

    }, false);

    self.getIntervals = function(peaks) {
        // What we now do is get all of our peaks, and then measure the distance to
        // other peaks, to create intervals.  Then based on the distance between
        // those peaks (the distance of the intervals) we can calculate the BPM of
        // that particular interval.

        // The interval that is seen the most should have the BPM that corresponds
        // to the track itself.

        var groups = [];

        peaks.forEach((peak, index) => {
            for (var i = 1;
                (index + i) < peaks.length && i < 10; i++) {
                var group = {
                    tempo: parseFloat(((60 * self.variables.sampleRate) / (peaks[index + i].position - peak.position)).toFixed(1)),
                    count: 1,
                    position: peak.position
                };

                if (group.tempo <= 0) {
                    return;
                }

                while (group.tempo < 100) { // 90
                    group.tempo *= 2;
                }

                while (group.tempo > 200) { // 180
                    group.tempo /= 2;
                }

                group.tempo = parseFloat((group.tempo).toFixed(1));

                if (!(groups.some(function(interval) {
                        return !!(interval.tempo === group.tempo ? interval.count++ : 0);
                    }))) {
                    groups.push(group);
                }
            }
        });
        return groups;
    }

    self.getPeaks = function(data) {
        // What we're going to do here, is to divide up our audio into parts.

        // We will then identify, for each part, what the loudest sample is in that
        // part.

        // It's implied that that sample would represent the most likely 'beat'
        // within that part.

        // Each part is 0.5 seconds long - or 22,050 samples. // -- CHANGED to 0.25sec

        // This will give us 60 'beats' - we will only take the loudest half of
        // those. //  -- CHANGED to quarter loudest

        // This will allow us to ignore breaks, and allow us to address tracks with
        // a BPM below 120.

        var partSize = self.variables.sampleRate / 4,
            parts = data[0].length / partSize,
            peaks = [];

        for (var i = 0; i < parts; i++) {
            var max = {
                position: 0,
                volume: 0
            };
            for (var j = i * partSize; j < (i + 1) * partSize; j++) {
                var volume = Math.max(Math.abs(data[0][j]), Math.abs(data[1][j]));
                if (!max || (volume > max.volume)) {
                    max = {
                        position: j,
                        volume: volume
                    };
                }
            }
            peaks.push(max);
        }

        // We then sort the peaks according to volume...
        peaks.sort(function(a, b) {
            return b.volume - a.volume;
        });

        // ...take the loundest half of those...
        peaks = peaks.splice(0, peaks.length * 0.25);

        // ...and re-sort it back based on position.
        peaks.sort(function(a, b) {
            return a.position - b.position;
        });

        return peaks;
    }
}
// This is in case of normal worker start
if (window != self)
    processAudioWorker();