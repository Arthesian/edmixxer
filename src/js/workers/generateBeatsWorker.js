function generateBeatsWorker() {
    // scope
    var self = this;

    // Variables to use over multiple functions
    self.variables = {}

    self.addEventListener('message', function(e) {
        // Process data here
        var returnObject = {
            Beats: []
        }

        var BPM = e.data.BPM;
        var beatTime = e.data.BeatDuration;
        var beatStartTime = e.data.BeatStartTime;
        var songDuration = e.data.SongDurationSeconds;

        var i = 0;
        for (var x = beatStartTime % beatTime; x < songDuration; x += beatTime) {

            i++;

            var beat = {};
            beat.Time = x;
            beat.BeatNumber = i;
            beat.BeatDuration = beatTime;
            beat.BeatNumber4th = i % 4;
            beat.BeatNumber8th = i % 8;
            beat.BeatNumber32th = i % 32;

            returnObject.Beats.push(beat);
        }

        self.postMessage(returnObject);

    }, false);
}
// This is in case of normal worker start
if (window != self)
    generateBeatsWorker();